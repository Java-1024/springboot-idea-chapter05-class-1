package com.itheima;

import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.thymeleaf.util.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CommonTest {
    @Test
    public void testfilename(){
        String filename="中文.jpg";
        String resultname= FilenameUtils.getBaseName(filename) + "."+FilenameUtils.getExtension(filename);
        System.out.println(resultname);
    }
    @Test
    public void testString(){
        String filename="abc";
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(filename+ ft.format(date));
    }

    @Test
    public void testEasyExcel(){

    }
}
