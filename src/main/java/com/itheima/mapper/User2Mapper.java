package com.itheima.mapper;
import com.itheima.domain.User2;


import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface User2Mapper {
    @Select("SELECT * FROM user2 WHERE id =#{id}")
    public  User2 findById(Integer id);

    @Select("SELECT * FROM user2")
    public List<User2> getAll();

//    @Insert("INSERT INTO teacher(jobno,username,age,mobile) " +
//            "values (#{jobno},#{username},#{age},#{mobile})")
//    public int addTeacher(Teacher teacher);
//
//    @Update("UPDATE teacher SET username=#{username} WHERE id=#{id}")
//    public int updateTeacher(Teacher teacher);
//
//    @Delete("DELETE FROM teacher WHERE id=#{id}")
//    public int deleteTeacher(Integer id);
}