package com.itheima.controller;

import com.itheima.domain.Teacher;
import com.itheima.domain.User2;
import com.itheima.service.TeacherService;
import com.itheima.service.User2Service;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: liaojinhu
 * @Date: 2021/10/25/0:03
 * @Description:
 */

@Controller
@RequestMapping("/user2")
public class User2Controller {
    @Autowired
    private User2Service user2Service;

    @GetMapping("/togetAll")
    public String togetAll(Model model){
        List<User2> userList = this.user2Service.getAll();
        model.addAttribute("user2List",userList);
        return "user2";
    }
}
