package com.itheima.service;

import com.itheima.domain.Teacher;
import com.itheima.mapper.TeacherMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {
    @Autowired
    private TeacherMapper teacherMapper;

    public List<Teacher> getAll(){
        return this.teacherMapper.getAll();
    }
}
