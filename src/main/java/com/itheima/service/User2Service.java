package com.itheima.service;


import com.itheima.domain.User2;
import com.itheima.mapper.User2Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class User2Service {
    @Autowired
    private User2Mapper user2Mapper;

    public List<User2> getAll(){
        return this.user2Mapper.getAll();
    }
}
